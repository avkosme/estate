<?php

namespace backend\controllers;

use Yii;
use yii\swiftmailer;
use yii\filters\AccessControl;

class MailController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]

        ];
    }

    public function actionIndex()
    {

        ////////////////////////////////////
        //  SEND EMAIL
        ///////////////////////////////////

        $db = Yii::$app->db2;
        $query = $db->createCommand('SELECT * FROM org')
            ->queryAll();

        foreach ($query as $t):
            \Yii::$app->mailer->compose('kordon-html',[
                'html' => 'kordon-html',
                'manager' => $t['manager'],
                'title' => $t['title']
            ])
                ->setFrom('volgodonsk@uckordon.ru')
                ->setTo($t['email'])
                ->setSubject('ООО УЦ Кордон Отчетность в сфере ЖКХ, ТСЖ в Волгодонске')
                ->setTextBody($t['manager'])
                ->send();
            //var_dump($t['email']);
            sleep(3);
        endforeach;

        ////////////////////////////////////
        //  SEND EMAIL
        ///////////////////////////////////

       return $this->render('index');
    }

}
