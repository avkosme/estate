<?php

namespace backend\controllers;

use Yii;
use app\models\VolgodonskKvartiryProdam;
use yii\helpers\Json;
use yii\web\Request;
use yii\web\Session;

class EstateController extends \yii\web\Controller
{
    public function actionIndex()
    {


        $session = new Session;
        $session->open();
        $session['queryParams'] = Yii::$app->request->post('VolgodonskKvartiryProdam');


        $searchModel = new \app\models\VolgodonskKvartiryProdamSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new VolgodonskKvartiryProdam();


        $SearchFormModel = new \app\models\SearchForm;


//echo '<br><br><br><br><br><br><br>';
        $i = 1;
        while($i <= 8)
        {
            $rooms_all_s[$i] = $i;
                $i++;
        }

        $i = 1;
        while($i <= 17)
        {
            $floor_all_s[$i] = $i;
                $i++;
        }




        $SearchFormModel->rooms_all_s = $rooms_all_s;
        $SearchFormModel->floor_all_s = $floor_all_s;
        return $this->render('index',
            [
                'SearchFormModel' => $SearchFormModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
    }

    public function actionJson()
    {


        $session = new Session;
        $session->open();

        $queryParams = $session['queryParams'];

        if (!empty($queryParams)) {

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $searchModel = new \app\models\VolgodonskKvartiryProdamSearch;

            $VolgodonskKvartiryProdam = $searchModel->search([$searchModel->formName() => $queryParams])->getModels();


        } else {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $VolgodonskKvartiryProdam = \app\models\VolgodonskKvartiryProdam::find()->orderBy(['_date' => SORT_DESC])->limit(300)->all();

        }


        return $VolgodonskKvartiryProdam;


        /*
                $VolgodonskKvartiryProdam = VolgodonskKvartiryProdam::find()->limit(1000)->all();


                foreach ($VolgodonskKvartiryProdam as $i):

                    $_id[] = $i['id'];
                    $an[] = $i['an'];

                endforeach;

                \Yii::$app->response->format = 'json';
                $items = ['_id' => $_id, 'an' => $an];


                //return $items;

                $items = [
                    {
                        "age": 0,
                "id": "motorola-xoom-with-wi-fi",
                "imageUrl": "img/phones/motorola-xoom-with-wi-fi.0.jpg",
                "name": "Motorola XOOM\u2122 with Wi-Fi",
                "snippet": "The Next, Next Generation\r\n\r\nExperience the future with Motorola XOOM with Wi-Fi, the world's first tablet powered by Android 3.0 (Honeycomb)."
            },
                ];

                return $items;*/
    }

}
