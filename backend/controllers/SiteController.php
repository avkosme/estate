<?php
namespace backend\controllers;

use app\models\VolgodonskKvartiryProdam;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','phone','getphone'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionPhone()
    {

        $request = Yii::$app->request;
        $data_id = $request->post('data_id');
        $_val = $request->post('_val');



        $VolgodonskKvartiryProdam = VolgodonskKvartiryProdam::find()->where(['iditem' => $data_id])->one();
        $VolgodonskKvartiryProdam->phone = $_val;
        $VolgodonskKvartiryProdam->save();
    }

    public function actionGetphone()
    {
        $request = Yii::$app->request;
        $data_id = $request->post('data_id');



        echo '<div class="input-group input-group-sm"><input type="text" class="form-control phone" placeholder="Телефон" value="'.$data_id.'" ></div><br><button type="button" class="btn btn-primary btn-xs send">Сохранить</button>';

    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
