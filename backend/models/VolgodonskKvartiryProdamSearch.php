<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VolgodonskKvartiryProdam;

/**
 * VolgodonskKvartiryProdamSearch represents the model behind the search form about `app\models\VolgodonskKvartiryProdam`.
 */
class VolgodonskKvartiryProdamSearch extends VolgodonskKvartiryProdam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iditem'], 'integer'],
            [['an', 'an_name', 'rooms', 'walls', 'price', 'area', 'floor', 'floors', 'address', '_desc', '_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VolgodonskKvartiryProdam::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => array('pageSize' => 300),
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /*  $query->andFilterWhere([
              'id' => $this->id,
              '_date' => $this->_date,
              'iditem' => $this->iditem,
          ]);*/


        //$query->limit(300);
        $query->andFilterWhere(['or like', 'rooms', $this->rooms])

            ->andFilterWhere(['or like', 'floors', $this->floors])
            ->andFilterWhere(['or like', 'floor', $this->floor])
            ->andFilterWhere(['or like', 'price', $this->price])
            ->andFilterWhere(['or like', 'area', $this->area])
            ->andFilterWhere(['or like', 'an', $this->an])
            ->andFilterWhere(['or like', '_desc', $this->_desc])
            ->andFilterWhere(['or like', 'walls', $this->walls])
            ->andFilterWhere(['or like', 'address', $this->address]);


        $query->orderBy(['_date' => SORT_DESC]);


        /*$query->filterWhere(['like', 'rooms', $this->rooms])
            ->orFilterWhere(['like', 'rooms', $this->rooms]);*/

       //// $query
            //->andFilterWhere(['like', 'an', $this->an])
          //  ->andFilterWhere(['like', 'an_name', $this->an_name])

           // ->andFilterWhere(['like', 'walls', $this->walls])

        //    ->andFilterWhere(['like', '_desc', $this->_desc])
       // ;


        return $dataProvider;
    }
}
