<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "volgodonsk_kvartiry_prodam".
 *
 * @property integer $id
 * @property string $an
 * @property string $an_name
 * @property string $rooms
 * @property string $walls
 * @property string $price
 * @property string $area
 * @property string $floor
 * @property string $floors
 * @property string $address
 * @property string $_desc
 * @property string $_date
 * @property integer $iditem
 * @property string $phone
 */
class VolgodonskKvartiryProdam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'volgodonsk_kvartiry_prodam';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_avito');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['an', 'an_name', 'rooms', 'walls', 'price', 'area', 'floor', 'floors', 'address', '_desc'], 'string'],
            [['_date'], 'safe'],
            [['iditem'], 'integer'],
            [['phone'], 'required'],
            [['phone'], 'string', 'max' => 200],
            [['iditem'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Квартиры в Волгодонске',
            'an' => 'Продавец',
            'an_name' => 'Имя продавца',
            'rooms' => 'Rooms',
            'walls' => 'Walls',
            'price' => 'Цена',
            'area' => 'Площадь',
            'floor' => 'Этаж',
            'floors' => 'Этажей',
            'address' => 'Адрес',
            '_desc' => 'Описание',
            '_date' => 'Дата добавления',
            'iditem' => 'Iditem',
            'phone' => 'Телефон',
        ];
    }
}