<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php
//var_dump($dataProvider);
?>



<div class="row">
    <div class="col-lg-12">

        <div class="collapse" id="collapseExample">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Комнат</label>
                        <br>
                        <?= Html::activeDropDownList($model, 'rooms', $SearchFormModel->rooms_all_s, ['class' => 'selectpicker', 'title' => 'Комнат', 'multiple' => 'multiple']) ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Этаж</label>
                        <br>

                        <?= Html::activeDropDownList($model, 'floor', $SearchFormModel->floor_all_s, ['class' => 'selectpicker', 'title' => 'Комнат', 'multiple' => 'multiple']) ?>


                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Этажей</label>
                        <br>
                        <?= Html::activeDropDownList($model, 'floors', $SearchFormModel->floor_all_s, ['class' => 'selectpicker', 'title' => 'Комнат', 'multiple' => 'multiple']) ?>


                    </div>

                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Площадь</label>
                        <?=
                        Html::activeTextInput($model, 'area', ['class' => 'form-control']);
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Цена</label>
                        <?=
                        Html::activeTextInput($model, 'price', ['class' => 'form-control']);
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Адрес</label>
                        <?=
                        Html::activeTextInput($model, 'address', ['class' => 'form-control']);
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Описание</label>
                        <?=
                        Html::activeTextInput($model, '_desc', ['class' => 'form-control']);
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Материал стен</label>
                        <?=
                        Html::activeTextInput($model, 'walls', ['class' => 'form-control']);
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Продавец</label>
                        <?=
                        Html::activeTextInput($model, 'an', ['class' => 'form-control']);
                        ?>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Имя продавца</label>
                        <?=
                        Html::textInput('', '', ['class' => 'form-control']);
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Дата добавления</label>
                        <?=
                        Html::textInput('', '', ['class' => 'form-control']);
                        ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <script>
                $('.selectpicker').selectpicker();
            </script>


        </div>

    </div>

</div>


<div class="row">


    <table class="table">

        <tr>
            <th class="text-center">Тип</th>
            <th class="text-center">Комнат</th>
            <th class="text-center">Этаж</th>
            <th class="text-center">Этажей</th>
            <th class="text-center">Площадь</th>
            <th class="text-center">Цена</th>

            <th class="text-center">Адрес</th>
            <th class="text-center">Описание</th>
            <th class="text-center">Материал стен</th>
            <th class="text-center">Продавец</th>
            <th class="text-center">Имя продавца</th>
            <th class="text-center">Дата добавления</th>
        </tr>

        <tr>
            <td class="text-center"></td>
            <td class="text-center"><input class="form-control" ng-model="query.rooms"></td>
            <td class="text-center"><input class="form-control" ng-model="query.floor"></td>
            <td class="text-center"><input class="form-control" ng-model="query.floors"></td>
            <td class="text-center"><input class="form-control" ng-model="query.area"></td>
            <td class="text-center"><input class="form-control" ng-model="query.price"></td>

            <td class="text-center"><input class="form-control" ng-model="query.address"></td>
            <td class="text-center"><input class="form-control" ng-model="query._desc"></td>
            <td class="text-center"><input class="form-control" ng-model="query.walls"></td>
            <td class="text-center"><input class="form-control" ng-model="query.an"></td>
            <td class="text-center"><input class="form-control" ng-model="query.an_name"></td>
            <td class="text-center"><input class="form-control" ng-model="query._date"></td>
        </tr>


        <tr ng-repeat="phone in phones | filter:query">


            <td class="text-center">
                <button type="button" class="btn {{phone.phone ? 'btn-success' : 'btn-danger'}}  btn-xs pop"
                        data-toggle="popover" data-placement="bottom"
                        data-id="{{phone.iditem}}" data-phone="{{phone.phone}}" data-content=" ">
                    <span class="glyphicon glyphicon-phone" aria-hidden="true"></span>
                </button>

                <button type="button" class="btn btn-primary btn-xs">
                    <span class="glyphicon glyphicon-stats" aria-hidden="true"></span>
                </button>
                <br>

                квартира <br>

                <?= Html::a('{{phone.iditem}}', 'http://anonym.to/?https://avito.ru/{{phone.iditem}}', ['target' => '_blank']); ?>


            </td>
            <td class="text-center">{{phone.rooms}}</td>
            <td class="text-center">{{phone.floor}}</td>
            <td class="text-center">{{phone.floors}}</td>
            <td class="text-center">{{phone.area}}</td>
            <td class="text-center">{{phone.price}}</td>
            <td class="text-center">{{phone.address}}</td>
            <td><p class="desc">{{phone._desc}}</p></td>
            <td class="text-center">{{phone.walls}}</td>
            <td class="text-center">{{phone.an}}</td>
            <td class="text-center">{{phone.an_name}}</td>
            <td class="text-center">{{phone._date}}</td>


        </tr>
    </table>

</div>

<script>

    $(document).ready(function () {
        $("body").on("click", ".pop", function () {


            var data_id = $(this).attr("data-id");
            /*var data_phone = $(this).attr("data-phone");*/


            $(function () {
                $('button.btn').popover({html: true})
            });

           $(function () {
               $('button.btn').on('shown.bs.popover', function () {
                   /*alert(data_id);*/
                   var _form = '<div class="input-group input-group-sm"><input type="text" class="form-control phone" placeholder="Телефон" value="" ></div><br><button type="button" class="btn btn-primary btn-xs send">Сохранить</button>';
                    $(this).attr("data-content", _form);


                   $.ajax({
                       type: "POST",
                       url: "/site/getphone",
                       data: {
                           data_id: data_id

                       }
                   })
                       .done(function (msg) {
                           var data_id = "";
                           $('.popover-content').replaceWith($('.popover-content').html(msg));

                       });


               });
           });





            /*Отправка AJAX запроса на сохранение*/
            /* $(function () {
             $("body").on("click", ".send", function () {

             var _val = $("input.phone").val();
             $.ajax({
             type: "POST",
             url: "/site/phone",
             data: {
             data_id: data_id,
             _val: _val

             }
             })
             .done(function (msg) {
             $('button.btn').popover('hide');

             });

             });
             });*/


        });


    });


</script>


