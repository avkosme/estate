<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-select.min.css',
        'css/site.css',
    ];

    public $js = [


        'js/controllers.js',
        'js/bootstrap-select.min.js',
        //'js/app.js',

        /*'js/angular.min.js',
        'js/bower_components/angular-route/angular-route.js',

        */
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\AngularAsset',
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
}
