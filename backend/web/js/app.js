'use strict';

/* App Module */

var phonecatApp = angular.module('phonecatApp', [
    'ngRoute',
    'phonecatControllers'
]);

phonecatApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/app', {
                templateUrl: 'partials/phone-list.html',
                controller: 'PhoneListCtrl'
            }).
            otherwise({
                redirectTo: '/'
                /*templateUrl: 'partials/phone-list.html',
                controller: 'PhoneListCtrl'*/
            });
    }]);
