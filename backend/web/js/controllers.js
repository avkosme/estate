'use strict';

/* Controllers */

var phonecatApp = angular.module('phonecatApp', []);

phonecatApp.controller('PhoneListCtrl', ['$scope', '$http', function ($scope, $http) {
    $http.get('/estate/json').success(function (data) {
        $scope.phones = data;
    });


}]);
