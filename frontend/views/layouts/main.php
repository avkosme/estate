<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Neucha&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Russo+One&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container">

    <div class="row">
        <div class="col-lg-12 text-right">
<p>
            <ul class="list-inline">
                <li>
                    <a href="#">Домашняя |</a>
                    <a href="#">Обо мне |</a>
                    <a href="#">Контакты</a>
                </li>
            </ul>
            </p>
        </div>
    </div>

    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>



    <footer class="footer">

        <div class="row">
            <div class="col-lg-3">

            </div>
            <div class="col-lg-6 text-center">
                <p>
                    Автор сайта - Костюченко Андрей Владимирович
                </p>
            </div>

            <div class="col-lg-3">

            </div>

        </div>


        <div class="row">

            <div class="col-lg-12 text-center">

                <ul class="list-inline">
                    <li>Главная</li>
                    <li>Обо мне</li>
                    <li>Контакты</li>
                </ul>
            </div>
            <br>
            <br>
            <br>
        </div>

    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
