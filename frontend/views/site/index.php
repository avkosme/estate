<?php
// этот код мы пропишем в отдельном view
use frontend\assets\FontAwesomeAsset;
FontAwesomeAsset::register($this);
/* @var $this yii\web\View */
$this->title = 'Андрей Костюченко - персональный сайт';
?>




<div class="row">
    <div class="col-lg-4">

        <div class="jumbotron">
            <h1>AvKosme</h1>

            <p class="pull-right">web - проекты играючи</p>

        </div>


    </div>

</div>
<div class="row">

    <div class="col-lg-8">


        <div id="carouselHacked" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="/images/tmp1.jpg">

                    <div class="carousel-caption">
                        <h3>Плато Лаго-Наки</h3>
                        <p>Республика Адыгея</p>
                        <a href="/site/page" class="text-white"><i class="fa fa-link"></i> Читать</a>

                    </div>

                </div>
                <div class="item">
                    <img src="/images/tmp2.jpg">

                    <div class="carousel-caption">
                        <h3>Плато Лаго-Наки</h3>
                        <p>Республика Адыгея</p>
                        <a href="/site/page" class="text-white"><i class="fa fa-link"></i> Читать</a>

                    </div>

                </div>
                <div class="item">
                    <img src="/images/tmp3.jpg">

                    <div class="carousel-caption">
                        <h3>Плато Лаго-Наки</h3>
                        <p>Республика Адыгея</p>
                        <a href="/site/page" class="text-white"><i class="fa fa-link"></i> Читать</a>

                    </div>

                </div>
                <div class="item">
                    <img src="/images/tmp4.jpg">

                    <div class="carousel-caption">
                        <h3>Плато Лаго-Наки</h3>
                        <p>Республика Адыгея</p>
                        <a href="/site/page" class="text-white"><i class="fa fa-link"></i> Читать</a>

                    </div>

                </div><div class="item">
                    <img src="/images/tmp5.jpg">

                    <div class="carousel-caption">
                        <h3>Плато Лаго-Наки</h3>
                        <p>Республика Адыгея</p>
                        <a href="/site/page" class="text-white"><i class="fa fa-link"></i> Читать</a>

                    </div>

                </div><div class="item">
                    <img src="/images/tmp6.jpg">

                    <div class="carousel-caption">
                        <h3>Плато Лаго-Наки</h3>
                        <p>Республика Адыгея</p>
                        <a href="/site/page" class="text-white"><i class="fa fa-link"></i> Читать</a>

                    </div>

                </div><div class="item">
                    <img src="/images/tmp7.jpg">

                    <div class="carousel-caption">
                        <h3>Плато Лаго-Наки</h3>
                        <p>Республика Адыгея</p>
                        <a href="/site/page" class="text-white"><i class="fa fa-link"></i> Читать</a>

                    </div>

                </div>

            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carouselHacked" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carouselHacked" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
<br>

    </div>

    <div class="col-lg-4">


        <h4 class="list-group-item-heading">Рубрики</h4>


        <ul class="list-unstyled">
            <li>
                <a href="#">Путешествия</a>
            </li>

        </ul>



    </div>



</div>