<?php

use yii\db\Schema;
use yii\db\Migration;

class m150429_091512_create_table_avito_db extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->execute("
CREATE DATABASE db_avito;
DROP TABLE IF EXISTS `db_avito`.`volgodonsk_kvartiry_prodam`;
CREATE TABLE  `db_avito`.`volgodonsk_kvartiry_prodam` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Квартиры в Волгодонске',
  `an` text COLLATE utf8_unicode_ci COMMENT 'Продавец',
  `an_name` text COLLATE utf8_unicode_ci COMMENT 'Имя продавца',
  `rooms` text COLLATE utf8_unicode_ci,
  `walls` text COLLATE utf8_unicode_ci,
  `price` text COLLATE utf8_unicode_ci COMMENT 'Цена',
  `area` text COLLATE utf8_unicode_ci COMMENT 'Площадь',
  `floor` text COLLATE utf8_unicode_ci COMMENT 'Этаж',
  `floors` text COLLATE utf8_unicode_ci COMMENT 'Этажей',
  `address` text COLLATE utf8_unicode_ci COMMENT 'Адрес',
  `_desc` text COLLATE utf8_unicode_ci COMMENT 'Описание',
  `_date` timestamp NULL DEFAULT NULL COMMENT 'Дата добавления',
  `iditem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `iditem` (`iditem`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");


        $this->execute("ALTER TABLE `db_avito`.`volgodonsk_kvartiry_prodam` ADD COLUMN `phone` VARCHAR(200) NOT NULL COMMENT 'Телефон' AFTER `iditem`;");

    }

    public function down()
    {
        echo "m150429_091512_create_table_avito_db cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
